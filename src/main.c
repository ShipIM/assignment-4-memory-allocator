#include "mem.h"
#include "test.h"
#include <inttypes.h>

int main() {
    size_t tests_passed = 0;
    
    for (size_t i = 0; i < test_counter; i++) {
        bool result = tests[i]();
        tests_passed += result;

        fprintf(result ? stdout : stderr, "%zu test %s\n", i + 1, result ? "passed" : "failed");
    }

    printf("Passed %zu tests out of %zu\n", tests_passed, test_counter);

    return 0;
}