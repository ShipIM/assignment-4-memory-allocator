#define _DEFAULT_SOURCE
#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

#define BLOCK_STANDARD_CAPACITY 1024
#define HEAP_SIZE 8192

static struct block_header *block_get_header(void *contents)
{
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

bool first_test(void)
{
    void *heap = heap_init(HEAP_SIZE);

    if (heap)
    {
        void *contents = _malloc(BLOCK_STANDARD_CAPACITY);

        if (contents)
        {
            munmap(heap, size_from_capacity((block_capacity){HEAP_SIZE}).bytes);

            return true;
        }

        munmap(heap, size_from_capacity((block_capacity){HEAP_SIZE}).bytes);
    }

    return false;
}

bool second_test(void)
{
    void *heap = heap_init(HEAP_SIZE);

    if (heap)
    {
        void *first = _malloc(BLOCK_STANDARD_CAPACITY);
        void *second = _malloc(BLOCK_STANDARD_CAPACITY);

        if (first && second)
        {
            struct block_header *header = block_get_header(second);
            size_t next_size = size_from_capacity(header->next->capacity).bytes;
            _free(second);

            if (header->is_free && (header->capacity).bytes == next_size + BLOCK_STANDARD_CAPACITY)
            {
                _free(first);
                munmap(heap, size_from_capacity(header->capacity).bytes);

                return true;
            }
        }

        munmap(heap, size_from_capacity((block_capacity){HEAP_SIZE}).bytes);
    }

    return false;
}

bool third_test(void)
{
    void *heap = heap_init(HEAP_SIZE);

    if (heap)
    {
        void *first = _malloc(BLOCK_STANDARD_CAPACITY);
        void *second = _malloc(BLOCK_STANDARD_CAPACITY);

        if (first && second)
        {
            struct block_header *second_header = block_get_header(second);
            size_t next_size = size_from_capacity(second_header->next->capacity).bytes;
            _free(second);

            if (second_header->is_free && (second_header->capacity).bytes == next_size + BLOCK_STANDARD_CAPACITY)
            {
                struct block_header *first_header = block_get_header(first);
                size_t next_size = size_from_capacity(first_header->next->capacity).bytes;
                _free(first);

                if (first_header->is_free && (first_header->capacity).bytes == next_size + BLOCK_STANDARD_CAPACITY)
                {
                    munmap(heap, size_from_capacity(first_header->capacity).bytes);

                    return true;
                }
            }
        }

        munmap(heap, size_from_capacity((block_capacity){HEAP_SIZE}).bytes);
    }

    return false;
}

bool fourth_test(void)
{
    void *heap = heap_init(HEAP_SIZE);

    if (heap)
    {
        void *first = _malloc(4 * BLOCK_STANDARD_CAPACITY);
        void *second = _malloc(8 * BLOCK_STANDARD_CAPACITY);

        if (second && first)
        {
            struct block_header *second_header = block_get_header(second);
            if (second_header->capacity.bytes == 8 * BLOCK_STANDARD_CAPACITY)
            {
                _free(second);
                _free(first);

                struct block_header *first_header = block_get_header(first);
                munmap(heap, size_from_capacity(first_header->capacity).bytes);

                return true;
            }
        }

        munmap(heap, size_from_capacity((block_capacity){HEAP_SIZE}).bytes);
    }

    return false;
}

bool fifth_test(void)
{
    void *heap = heap_init(HEAP_SIZE);

    if (heap)
    {
        void *first = _malloc(6 * BLOCK_STANDARD_CAPACITY);

        if (first)
        {
            struct block_header *first_header = block_get_header(first);
            void *result = mmap(first_header->next->contents + first_header->next->capacity.bytes, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

            if (result != MAP_FAILED)
            {
                void *second = _malloc(8 * BLOCK_STANDARD_CAPACITY);

                if (second)
                {
                    struct block_header *second_header = block_get_header(second);
                    if (second_header->capacity.bytes == 8 * BLOCK_STANDARD_CAPACITY && second_header != (void*)(first_header->next->contents + first_header->next->capacity.bytes))
                    {
                        _free(second);
                        munmap(result, HEAP_SIZE);
                        _free(first);

                        munmap(heap, size_from_capacity(first_header->capacity).bytes);
                        munmap(heap, size_from_capacity(second_header->capacity).bytes);

                        return true;
                    }
                }
            }
        }

        munmap(heap, size_from_capacity((block_capacity){HEAP_SIZE}).bytes);
    }

    return false;
}

size_t test_counter = 5;
bool (*tests[5])() = {
    first_test,
    second_test,
    third_test,
    fourth_test,
    fifth_test
};