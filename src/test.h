#include <stdbool.h>
#include <stdio.h>

typedef bool test (void);

extern bool (*tests[])();

extern size_t test_counter;